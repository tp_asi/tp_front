# TP_FRONT

Front end part of the intervention application.

## RUNNING THE DEVELOPMENT WEB SERVER

### REQUIREMENTS

- NodeJS
- NPM
- Git
- A command line interface
- An internet browser

### STEPS

- Use the following command to clone the repository `git clone https://gitlab.com/tp_asi/tp_front.git`
- Go inside the directory with `cd tp_front`
- Run the following commands `npm install`, `npm start`

The application is now available in your browser at the following address : `http://localhost:4200`

### RUNNING THE TESTS

You can run the tests using the following command : `npm test`
