'use strict';

describe('Intervention', function() {

  var $httpBackend;
  var Intervention;

  // Add a custom equality tester before each test
  beforeEach(function() {
    jasmine.addCustomEqualityTester(angular.equals);
  });

  beforeEach(inject(function(_$httpBackend_, _Intervention_) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('http://localhost:8000/interventions/interventions-view/?is_draft=true&date=ascending').respond();

    Intervention = _Intervention_;
  }));

});