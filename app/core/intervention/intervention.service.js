'use strict';

angular.
  module('core.intervention', ['ngResource']).
  factory('Intervention',

  /**
   * This service is used to build all the REST http request for the intervention backend.
   * @param {Resource} $resource - Resource used to produce the REST requests to the backend.
   */
  function($resource) {
      return $resource('http://localhost:8000/interventions/interventions-view/:id', {id:'@id'}, {
          queryList: {
            method: 'GET',
            params: {is_draft: '@isDraft', date: '@orderMode'},
            isArray: true
          },
          create: {
            method: 'POST',
            headers: {'Content-Type': 'application/json'}
          },
          update: {
            method: 'PUT',
            headers: {'Content-Type': 'application/json'}
          },
          delete: {
            method: 'DELETE'
          }
        })
    });