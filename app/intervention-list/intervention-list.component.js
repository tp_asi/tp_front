'use strict';

angular.
  module('interventionList').
  component('interventionList', {
    templateUrl: 'intervention-list/intervention-list.template.html',
    controller: ['Intervention',
    /**
     * Controller of the intervention for the web view.
     * @param {Intervention} Intervention - Service used to build the http REST requests to the interventions backend.
     * @property {string} orderMode - Used to dertermine the ordering of the intervention list regarding their dates.
     * @property {array} interventions - The list of the interventions currently displayed.
     * @property {bool} isDraft - The current status of the interventions displayed.
     * @property {bool} isModalDisplayed - Determine whether or not the intervention's creation / edition modal is displayed.
     * @property {bool} canUpdate - Determine whether or not the intervention can be updated.
     * @property {object} interventionDetails - Represents the intervention currently edited or created.
     */
      function InterventionListController(Intervention) {

        this.orderMode = 'ascending';
        this.interventions = Intervention.queryList({is_draft: false, date: this.orderMode});
        this.isDraft = false;
        this.isModalDisplayed = false;
        this.canUpdate = true;
        this.interventionDetails;
        
        /**
         * Fetch the list of interventions, the results
         * will be assigned to this.interventions.
         * @param {bool} $isDraft - Determine if the interventions fetched will be the drafts or not.
         */
        this.getInterventions = function getInterventions($isDraft) {
          this.interventions = Intervention.queryList({is_draft: $isDraft, date: this.orderMode});
          this.isDraft = $isDraft;
        };
        
        /**
         * Set the ordering parameter of the interventions
         * regarding their dates and updates the list.
         * @param {string} $mode - String that represents the order mode for the interventions dates (ascending or descending).
         */
        this.orderDate = function orderDate($mode) {
          this.orderMode = $mode;
          this.getInterventions(this.isDraft);
        }

        /**
         * Display the creation / edition modal of an intervention.
         * If $intervention is null, the modal is set to create mode.
         * @param {object} $intervention - Object that contains all the current intervention data. 
         */
        this.displayModal = function displayModal($intervention) {
          // Lots of nulls and "if branches" here, I couldn't spend more time to
          // find a cleaner way of handling the date field so this will do for now.

          if ($intervention != null) {
            this.interventionDetails = $intervention;
            this.isUpdating = true;
            this.setRequestStatus($intervention.date);
          } else {
            this.interventionDetails = null;
            this.isUpdating = false;
            this.interventionDate = null;
          }
          this.isModalDisplayed = true;
        };
        
        /**
         * Used to close the creation / edition modal.
         */
        this.closeModal = function closeModal() {
          this.isModalDisplayed = false;
        }

        /**
         * Sends the create request to the backend with this.interventionDetails
         * as the POST body and update the intervention list.
         */
        this.create = function create() {
          if (this.interventionDate != null) {
            this.interventionDetails.date = this.yyyymmdd(this.interventionDate);
          }
          var self = this;
          Intervention.create(this.interventionDetails).$promise
          .then(function() {
            self.isModalDisplayed = false;
            self.getInterventions(self.isDraft);
          })
          .catch(function(error) {
            console.log(error); // this is how a true chad logs an error
          });
        }

        /**
         * Sends the update request to the backend with this.interventionDetails
         * as the POST body and update the intervention list.
         */
        this.update = function update() {
          if (this.interventionDate != null) {
            this.interventionDetails.date = this.yyyymmdd(this.interventionDate);
          }
          var self = this;
          Intervention.update({id: this.interventionDetails.id}, this.interventionDetails).$promise
          .then( function() {
            self.isModalDisplayed = false;
            self.getInterventions(self.isDraft);
          })
          .catch(function(error) {
            console.log(error);
          });
        }
        
        /**
         * Sends the delete request to the backend with the ID of this.interventionsDetails
         * as a target and update the intervention list.
         */
        this.del = function del() {
          var self = this;
          Intervention.delete(this.interventionDetails).$promise
          .then(function() {
            self.isModalDisplayed = false;
            self.getInterventions(self.isDraft);
          })
          .catch(function(error) {
            console.log(error);
          });
        }
        
        /**
         * Sets the request status of the currently targeted intervention regarding its date.
         * @param {string} interventionDate - The date of the current intervention.
         */
        this.setRequestStatus = function setRequestStatus(interventionDate) {
          if (interventionDate !== null && interventionDate !== '') {
            this.interventionDate = new Date(interventionDate);
            let currentDate = new Date();
            
            if (currentDate.getTime() > this.interventionDate.getTime()) {
              this.requestDisabled = true;
              return;
            }
          }
          this.requestDisabled = false;
        }

        /**
         * Used to format a date object into a string.
         * @param {Date} dateIn - Date to convert.
         * @returns {string} - Date with a string format.
         */
        this.yyyymmdd = function yyyymmdd(dateIn) {
          var yyyy = dateIn.getFullYear();
          var mm = dateIn.getMonth() + 1;
          var dd = dateIn.getDate();
          return String(yyyy) + '-' + String(mm) + '-' + String(dd);
        }

      }
    ]
  });
