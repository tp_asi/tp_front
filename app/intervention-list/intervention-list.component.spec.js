'use strict';

describe('interventionList', function() {

  beforeEach(module('interventionList'));

  describe('InterventionListController', function() {
    var $httpBackend, ctrl;

    beforeEach(inject(function($componentController, _$httpBackend_) {
      $httpBackend = _$httpBackend_;
      $httpBackend.expectGET('localhost:8000/interventions/intervnetions-view?date=ascending&is_draft=false')
                  .respond([
                    {
                      id: 0,
                      label: 'intervention 1',
                      description: 'description 1',
                      serviceman_name: 'intervenant 1',
                      place_name: 'lieu 1',
                      date: '2021-01-01',
                      is_draft=false
                    },
                    {
                      id: 1,
                      label: 'intervention 2',
                      description: 'description 2',
                      serviceman_name: 'intervenant 2',
                      place_name: 'lieu 2',
                      date: '2021-01-02',
                      is_draft=false
                    }
                  ]);

      ctrl = $componentController('interventionList');
    }));

    it('should create a `interventions` property with 2 interventions fetched with `$http`', function() {
      jasmine.addCustomEqualityTester(angular.equals);

      expect(ctrl.interventions).toEqual([]);
      $httpBackend.flush();
      expect(ctrl.interventions.length).toEqual(2);
    });

  });

});

