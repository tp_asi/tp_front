'use strict';

angular.
  module('interventionApp').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/intervention', {
          template: '<intervention-list></intervention-list>'
        }).
        otherwise('/intervention');
    }
  ]);