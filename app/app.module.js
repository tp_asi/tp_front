'use strict';

angular.module('interventionApp', [
    'ngRoute',
    'core',
    'interventionList'
]);